import java.util.*;
public class HashMapEx {
    public static void main(String args[]) {
        HashMap hm = new HashMap();
        hm.put("C-336", "DDD");
        hm.put("T-552", "PPP");
        hm.put("N-152", "AEB");
        hm.put("F-123", "ABC");
        hm.put("S-143", "ADC");
        Iterator i = hm.keySet().iterator();
        while (i.hasNext()) {
            Object key = i.next();
            String s = key.toString();
            String k[] = s.split("-");
            int val = Integer.parseInt(k[1]);
            if (val % 3 == 0) {
                i.remove();
            }
        }
        System.out.println(hm.size());
    }
}
